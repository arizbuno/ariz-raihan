class Cartecian
  def cartecian_length(x1, y1, x2, y2)
    deltax = x1-x2
    deltay = y1-y2
    Math.sqrt(deltax**2 + deltay**2)
  end
end