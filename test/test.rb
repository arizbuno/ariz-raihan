require './lib/cartecian.rb'

describe Cartecian do
  it 'Should be return the length of cartecian from point (0,0) to (0,1), the expected output is 1' do
    expect(Cartecian.new.cartecian_length(0,0,0,1)).to eq(1)
  end

  it 'Should be return the length of cartecian from point (0,0) to (3,4), the expected output is 5' do
    expect(Cartecian.new.cartecian_length(0,0,3,4)).to eq(5)
  end

end